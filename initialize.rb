class Book
  attr_accessor :title, :author, :pages
  def initialize(title, author, pages)      #<--- can give it parameters    #everytime book.new is run this gets called
      @title = title      # this refers to the title attribute
      @author = author
      @pages = pages
  end                     #  in the acessor meaning title of object creating equals ttitle input
end




book1 =  Book.new("Harry Potter", " J.K Rowling", 400)
puts book1.author
puts book1.title

book2 = Book.new("Lord of the Rings", "Tolkein", 600)
  puts book2.title
puts book2.author
