#If statements special
#structure to help make decisons and respond to other bits of information within the code

#everyday if statements

=begin
i wake up
if im hungry #conditon (true or false)
  i eat breakfast #if condition true will action


  i leave my house
  if its cloudy (condition)
    i will bring and umbrella  (if condition is true will bring umbrella)
    otherwise (if condition is not true will execute this)
    i bring sunglasses

    im at a restaurant
    if i want meant  (condition)
      i order steak
      otherwise if i want pasta (condition)
      i order spaghetti & meatballs
      otherwise (condition)
      i order salad
=end


ismale = false # variable with value
istall = true  # second variable
if ismale and istall #as long as this is true then the code inside is executed ///
  #can use AND, OR// and means both need to be true whilst Or means only 1 needs to be true
puts " You Are Male and tall"
elsif ismale and !istall # shorthand for not tall (negation marker)
  puts "you are a short male "
elsif !ismale and istall   #elsif can catch all of the different possibilites
  puts " you are not male but are tall"
else
  puts " You are a short female "
end
#change the variable to play arropund with it
# if statements read through and exectues what meets the crieteriaa for that line of code
