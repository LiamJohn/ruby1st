lucky_nums = [4, 12, 19, 45, 71]




begin    #code inside begin and rescue is what  ca break the programme

#num = 10/0 #---------division by zero error

lucky_nums["dogs"] #--------- string doest fit array /// type error


rescue ZeroDivisionError   # whatever is between rescue and end this will be exectued

puts " division by 0 error"

rescue TypeError => e          # can use e as the error variable and print it out

  puts e


end


#advised to put whatever error type it is e.g. ZeroDivisionError or TypeError
