###very important lesson##

#can create own data types!!!1 can be used to represnt other things (Classes=custom data type)

# example of a book

class Book        #class then name/ basicalluy designing a book
  attr_accessor :title, :author, :pages       # attribute acessor is information for data type
end                                         #essentially laying the blueprint for a book, this is what they need

# after declaring whats required for that data you can create
#  indicuals "books" (classes) done very similar to variable

book1 =  Book.new()   # telling ruby we want a new book inside the program as book1 var
book1.title = "Harry Potter"
book1.author = "J.K Rowling"      # these are the attributes for that book
book1.pages = 400

puts book1.title
puts book1.pages
puts book1.author

book2 = Book.new
book2.title = "Lord of the Rings"
book2.author = "Tolkein"
book2.pages = 600

puts book2.title
puts book2.author   # can call any of the data from our class
puts book2.pages
# everything in ruby is a class or objecrt really usefull!!!!!

=begin
intialize methods -
had to manually type lines 14-17 //// 23-26
shortcut instead of having to declare every indvidual object
=end
