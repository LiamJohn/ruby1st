def get_day_name (day) # defineing method and abbreviation
  day_name = ""      # blank text so string input enters



# when statement ---------------
  case day
when "mon"
  day_name = "Monday"
when "tue"
  day_name = "Tuesday"
when  "wed"
  day_name = "Wednesday"
when "thu"
  day_name = "Thursday"
when "fri"
  day_name = "Friday"
when "sat"
  day_name = "Saturday"
when "sun"
  day_name = "Sunday"

else
  day_name = "invalid abbreviation"

  return day_name
  end
end


puts get_day_name("tues")

#appropriate for checking the same value agianst multiple others and running an action

=begin
if day == "mon"
  day_name = "monday"
elsif day == "tues"
  day_name = "tueday"
  -----------------------would be the longwinded way so use case  expression
=end
