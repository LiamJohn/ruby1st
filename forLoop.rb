#for loops allows to to work through a collection of something /
#this page consists of diffeent looping structures

friends =[  "Kevin", "Mark", "David", "Angela", "oscar"]  # standard array

=begin

for friend in friends # this would search everyone in the array nd print them
  puts friend
end

=end

=begin
friends.each do |friend|
  puts friend
end
=end

for index in 0..5  # used to go through loop stated amount  of times
  puts index
end
#-----------------
6.times do |index| # this does the same as line 15 except tates how many time to run the loop 
  puts index
end
